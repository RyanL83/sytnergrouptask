﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SytnerCars.Models
{
    public class Cars
    {
        public int ID { get; set; }
        public string CarMake { get; set; }
        public string CarModel { get; set; }
        public string CarEngine { get; set; }
        public double CarEngineSize { get; set; }
        public int CarModelYear { get; set; }
    }
}