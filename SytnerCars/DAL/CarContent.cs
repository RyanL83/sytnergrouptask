﻿using SytnerCars.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace SytnerCars.DAL
{
    public class CarContent : DbContext
    {

        public CarContent() : base("CarContent")
        {
        }

        public DbSet<Cars> Cars { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }

}