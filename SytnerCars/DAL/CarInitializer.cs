﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using SytnerCars.Models;

namespace SytnerCars.DAL
{
    public class CarInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<CarContent>
    {
        protected override void Seed(CarContent context)
        {
            var cars = new List<Cars>
            {
            new Cars{CarMake="Ford",CarModel="Fiesta",CarEngine="Straight-Six",CarEngineSize=2.5,CarModelYear=2015},
            new Cars{CarMake="Jaquar",CarModel="XJ",CarEngine="Straight-Six",CarEngineSize=4,CarModelYear=2010},
            new Cars{CarMake="Ford",CarModel="KA",CarEngine="Straight-Six",CarEngineSize=1.5,CarModelYear=2006},
            };

            cars.ForEach(s => context.Cars.Add(s));
            context.SaveChanges();
        }
    }
}